#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 30 10:59:07 2022

@author: poulpe
"""
# script pour produire les rasters de qualité GET
import rasterio as rio
import sys
import glob
import os

# on ajoute le chemin vers le script d'inversion du GET
sys.path.append('/home/poulpe/SIG/Seine/qualite_sat/code_GET')
# on importe ce script d'inversion
import Inversion

# le répertoire père contenant les bandes à traiter
path_sat = '/home/poulpe/SIG/Seine/qualite_sat/sat/31UCQ_img/2019_no_clouds/'

# le répertoire d'export des bandes de qualité calculées
path_rsl = '/home/poulpe/SIG/Seine/qualite_sat/calc_qual_get/'

# on créé une instance de la classe d'inversion
inv = Inversion.DWInversionAlgos()

# on créé une liste vide qui contiendra les sous répertoires des bandes
lst_dates = []

# on parcourt le répertoire père
for f in sorted(os.listdir(path_sat)):
    # on récupère dans une liste les répertoires *.data* contenant les bandes
    if '.data' in f:
        lst_dates.append(f)

# on parcourt la liste des sous réperoires i.e. la liste des dates
for im in lst_dates:
    # on récupère la date
    date = im[:21][len(im[:21])-8:]
    
    # on récupère les bandes d'intérêt
    # Blue
    path_blue = glob.glob(path_sat + im + '/Rrs_B2.img')
    rast_blue = rio.open(path_blue[0])
    band_blue = rast_blue.read(1)
    
    # Green
    path_green = glob.glob(path_sat + im + '/Rrs_B3.img')
    rast_green = rio.open(path_green[0])
    band_green = rast_green.read(1)
    
    # Red
    path_red = glob.glob(path_sat + im + '/Rrs_B4.img')
    rast_red = rio.open(path_red[0])
    band_red = rast_red.read(1)
    
    # Red Edge 1
    path_red_edge_1 = glob.glob(path_sat + im + '/Rrs_B5.img')
    rast_red_edge_1 = rio.open(path_red_edge_1[0])
    band_red_edge_1 = rast_red_edge_1.read(1)
    
    # Red Edge 2
    path_red_edge_2 = glob.glob(path_sat + im + '/Rrs_B6.img')
    rast_red_edge_2 = rio.open(path_red_edge_2[0])
    band_red_edge_2 = rast_red_edge_2.read(1)
    
    # NIR
    path_nir = glob.glob(path_sat + im + '/Rrs_B8A.img')
    rast_nir = rio.open(path_nir[0])
    band_nir = rast_nir.read(1)
    
    ############## aCDOM_brezonik
    # on calcule le aCDOM_brezonik
    aCDOM_brezonik = inv.aCDOM_brezonik(band_blue, band_red)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_aCDOM_brezonik.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=aCDOM_brezonik.shape[0],
         width=aCDOM_brezonik.shape[1],
         count=1,
         dtype=aCDOM_brezonik.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(aCDOM_brezonik, 1)
    raster_export.close()
    
    ############## aCDIM_perso
    # on calcule le aCDIM_perso
    aCDOM_perso = inv.aCDOM_perso(band_green, band_red)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_aCDOM_perso.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=aCDOM_perso.shape[0],
         width=aCDOM_perso.shape[1],
         count=1,
         dtype=aCDOM_perso.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(aCDOM_perso, 1)
    raster_export.close()
    
    ############## aCDOM_alcantara
    # on calcule le aCDOM_alcantara
    aCDOM_alcantara = inv.aCDOM_alcantara(band_red, band_blue)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_aCDOM_alcantara.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=aCDOM_alcantara.shape[0],
         width=aCDOM_alcantara.shape[1],
         count=1,
         dtype=aCDOM_alcantara.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(aCDOM_alcantara, 1)
    raster_export.close()
    
    ############## chl_giteslon
    # on calcule le chl_giteslon
    chl_giteslon = inv.chl_giteslon(band_red, band_red_edge_1, band_red_edge_2)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_chl_giteslon.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=chl_giteslon.shape[0],
         width=chl_giteslon.shape[1],
         count=1,
         dtype=chl_giteslon.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(chl_giteslon, 1)
    raster_export.close()
    
    ############## chl_lins
    # on calcule le chl_lins
    chl_lins = inv.chl_lins(band_red, band_red_edge_1)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_chl_lins.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=chl_lins.shape[0],
         width=chl_lins.shape[1],
         count=1,
         dtype=chl_lins.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(chl_lins, 1)
    raster_export.close()
    
    ############## SPM_GET
    # on calcule le SPM_GET
    SPM_GET = inv.SPM_GET(band_red, band_nir)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_SPM_GET.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=SPM_GET.shape[0],
         width=SPM_GET.shape[1],
         count=1,
         dtype=SPM_GET.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(SPM_GET, 1)
    raster_export.close()
    
    ############## SPM_GET (fictif, tambouille pour R...)
    # on calcule le SPM_GET
    SPM_GET = inv.SPM_GET(band_red, band_nir)
    
    # on créé le chemin d'export avec un nom qui convient
    path_export = path_rsl + date + '_zSPM_GET.tif'
    
    # on exporte le raster
    raster_export = rio.open(
         path_export, # on définit le chemin d'export
         'w',
         driver='GTiff',
         height=SPM_GET.shape[0],
         width=SPM_GET.shape[1],
         count=1,
         dtype=SPM_GET.dtype,
         crs=rast_green.crs,
         transform=rast_green.transform,
         )
    # enfin on écrit dans ce raster la bande à exporter
    raster_export.write(SPM_GET, 1)
    raster_export.close()
    
    
    #break
print('OK')